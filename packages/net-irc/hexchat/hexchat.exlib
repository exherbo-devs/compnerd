# Copyright 2014 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm; then
    SCM_SECONDARY_REPOSITORIES="flatpak_shared_modules"
    SCM_EXTERNAL_REFS="flatpak/shared-modules:flatpak_shared_modules"
    SCM_flatpak_shared_modules_REPOSITORY='https://github.com/flathub/shared-modules'
fi

require lua [ multibuild=false with_opt=true ]
require python [ blacklist="2" multibuild=false with_opt=true ]
require github [ tag="v${PV}" ]
require freedesktop-desktop gtk-icon-cache
require meson

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="GTK+ IRC client"
DESCRIPTION="
HexChat is an IRC client based on XChat, but unlike XChat it's completely free for both
Windows and Unix-like systems. Since XChat is open source, it's perfectly legal.
"
HOMEPAGE="https://${PN}.github.io"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    dbus [[ description = [ Used for single-instance and scripting interface ] ]]
    libcanberra [[ description = [ Support for sound alerts ] ]]
    lua [[ description = [ Support for Lua plugins ] ]]
    perl [[ description = [ Support for Perl plugins ] ]]
    python [[ description = [ Support for Python plugins ] ]]
    spell
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.6]
        virtual/pkg-config
    build+run:
        app-text/iso-codes
        dev-libs/glib:2[>=2.36.0]
        sys-apps/pciutils [[ note = [ Used by sysinfo plugin ] ]]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:2[>=2.24.0]
        x11-libs/libX11
        x11-libs/pango
        dbus? (
            dev-libs/dbus-glib:1[>=0.60]
            sys-apps/dbus[>=0.60]
        )
        libcanberra? ( media-libs/libcanberra[>=0.22] )
        perl? ( dev-lang/perl:= )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:=[>=0.9.8] )
        spell? ( app-spell/enchant:2 )
"

# Tests only validate desktop and appdata.xml files
# but appstream-util tries to use internet for this.
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dgtk-frontend=true
    -Dtext-frontend=false
    -Dtls=enabled
    -Dwith-checksum=false
    -Dwith-fishlim=false
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    "perl with-perl"
    "lua with-lua lua-${LUA_ABIS}"
    "python with-python python-${PYTHON_ABIS}"
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    "dbus"
    "libcanberra"
)

hexchat_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

hexchat_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

